package tedak;

public class TedakListener implements Runnable {

	TedakGUI gui;
	TedakWrapper tw;

	public TedakListener(TedakGUI g, TedakWrapper t) {
		this.gui = g;
		this.tw = t;
	}

	@Override
	public void run() {
		byte[] pkt;

		while (true) {
			pkt = tw.tk_recv();

			// Check CRC, type, etc.
			gui.receiveMessage("Someone",(new String(pkt)).substring(8));
		}

	}


}