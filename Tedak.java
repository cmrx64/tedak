import java.lang.String;

public class TedakWrapper {

	private native int tk_init(String server, String port);
	private native int tk_pack(char type, int size, byte[] body, byte[] ret);
	private native int tk_send(byte[] pkt);
	private native int tk_send(char type, int size, byte[] body);

	static {
		System.loadLibrary("tedak");
	} 

	public Tedak(String nserv, String nport) {
		this.tk_init(nserv,nport);
	}

	public int send(String message) {
		byte[] pkt = new byte[500];

		this.tk_send((char)16,message.length(),message.getBytes());
		return 1;
	}
}