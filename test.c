#include "tedak.h"

#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
unsigned char READY = 0;
char username[32] = {0};
extern int outSock;


void handle_sigint(int x)
{
	fprintf(stdout,"Interrupt signal received!\nQuitting...\n\n");
	close(outSock);
	exit(1);
}

void handle_sigpipe(int x)
{
	fprintf(stderr,"Received SIGPIPE, ignoring...\n");
}

int run_command(char* input)
{
	tk_packet_t pk;

	if (!strncmp(input,"quit",4))
		return 1;
	else if (!strncmp(input,"login",4))
	{
		strncpy(username,&input[6],
			(strlen(&input[6]) >= 32) ? 31 : strlen(&input[6]));
		tk_pack(PK_LOGIN,strlen(username),username,&pk);
		tk_send(&pk);
		fprintf(stdout,"Sending login information for user '%s'\n",&input[6]);
		// WAIT FOR RESPONSE HERE
		READY = 1;
		return 1;
	}
	else 
	{
		fprintf(stderr,"Command '%s' not found!\n",input);
		return 1;
	}
	// SHOULD NOT REACH THIS
	return -1;
}

int main(int argc, char** argv)
{
	char input[255];
	tk_packet_t pck;

	if (argc != 3)
	{
		printf("Usage: %s <address> <port>\n",argv[0]);
		return 1;
	}

	signal(SIGINT,handle_sigint);
	signal(SIGPIPE,handle_sigpipe);

	tk_init(argv[1],argv[2]);

	while (strncmp(input,"/quit",6))
	{
		fgets(input,255,stdin);
		input[strlen(input)-1] = '\0';

		if ('/' == input[0])
		{
			if (run_command(&input[1])) continue;
			else break;

		}
		if (READY)
		{
			memset(&pck,0,sizeof(tk_packet_t));
			tk_pack(PK_MSG,strlen(input),input,&pck);
printf("size sent in: %d\n",strlen(input));

			printf("sent %d bytes\n",tk_send(&pck));
		}
		else 
		{
			fprintf(stderr,"You are not logged in!\n");
		}
	}

	tk_close();
	return 0;
}